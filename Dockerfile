FROM node:carbon

WORKDIR /adoocore

COPY package*.json ./

RUN npm i
RUN npm i nodemon -g

COPY . .

EXPOSE 8080

CMD ["nodemon", "app.js"]

# BUILDING DOCKER
# docker build -t adoo1/coreone:latest .
# docker run -p 90:80 --name test adoo1/frontone.st