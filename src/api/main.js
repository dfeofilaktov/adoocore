const Router = require('koa-router');

const log = require('../utils/logger.js');
const { version } = require('../../package.json');
const { startApp } = require('../services/appService.js');

const mainApi = new Router({ prefix: '/core' });

/**
 * Ping request with version response
 * @example POST: adooone.com/core/ping?
 */
mainApi.get('/ping', async (ctx, next) => {
    log.info(version);

    const response = { msg: 'pong', core: { version } };
    await next();
    
    // response
    ctx.body = response;
});

module.exports = mainApi;
