const log = require('../utils/logger.js');

const startApp = async function startApp(ctx) {
    log.debug('In startApp!');
};

module.exports = {
    startApp,
};