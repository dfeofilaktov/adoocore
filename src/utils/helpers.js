/* eslint-disable no-console */
const chalk = require('chalk');
const log = require('../utils/logger.js');

const getInfo = (req) => {
    log.info('** REQUEST ********************');
    log.info(chalk.bgGray(req.originalUrl));
    log.info(`params: ${JSON.stringify(req.query, null, 4)}`);
    log.info(`body: ${JSON.stringify(req.body, null, 4)}`);
    log.info(
        chalk.italic('Request from:'),
    );
    log.info(
        chalk.gray(' ip > ')
        + chalk.black(chalk.bgCyan(`${req.ip || req.ips}`)),
    );
    log.info('** END *************************');
};

function listeningReporter() {
    const { address, port } = this.address();
    log.info(`App is on http://${address}:${port}`);
}

module.exports = {
    getInfo,
    listeningReporter,
};
