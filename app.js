require('dotenv').config();

const http = require('http');
// Koa Modules Section
const Koa = require('koa');
const helmet = require('koa-helmet');
const compress = require('koa-compress');
const mount = require('koa-mount');
const serveList = require('koa-serve-list');
const serveStatic = require('koa-better-static2');

const log = require('./src/utils/logger.js');
const api = require('./src/api/main.js');

const app = new Koa();

app.use(helmet());
app.use(compress());
app.use(api.routes()).use(api.allowedMethods());

const staticServ = new Koa();
const appLogDir = './log';
staticServ.use(serveStatic(appLogDir, { maxage: 10 }));
staticServ.use(serveList(appLogDir, {}));

app.use(mount('/logs', staticServ));

const {
    startApp,
} = require('./src/services/appService.js');
const { listeningReporter } = require('./src/utils/helpers.js');

(async function() {
    http.createServer(app.callback()).listen(8080, '0.0.0.0', listeningReporter);
    log.info('App is starting');
    await startApp(app.context);
}());
